# Deep Learning Specialisation



## Course Structure

- Course 1: Neural Network and Deep Learning
- Course 2: Improving Deep Neural Networks
- Course 3: Convolution Neural Network
- Course 4: Sequence Model
